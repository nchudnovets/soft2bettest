const express = require("express");
const bodyParser = require("body-parser");

const app = express();
const router = require("./routes");

app.use(bodyParser.json());
app.use('/', router);


app.listen(process.env.PORT || 4545, ()=>{
  console.log("MediatorService started.");
});
