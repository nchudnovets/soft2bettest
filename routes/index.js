const express = require('express');
const Joi = require("joi");
const axios = require("axios");
const servicesList = require("../data/services");

const router = express.Router();


router.get('/help', (req, res) => {
  const help = servicesList.length > 0
                ?
                  servicesList.map((service)=>{
                    const serviceDescription = {
                      [service.type]: service.description,
                      actions: []
                    };
                    service.actions.forEach((action)=>{
                      serviceDescription.actions.push(
                        {
                          [action.actionName]: {
                            params: action.params
                          }
                        }
                      );
                    });
                    return serviceDescription;
                  })
                :
                  "Can't help. There are no services yet";
  return res.json(help);
});



router.post('/services/create', (req, res) => {
  const schema = {
    name: Joi.string().min(3).required(),
    type: Joi.string().required(),
    description: Joi.string().required(),
    host: Joi.string().required(),
    actions: Joi.array().items(
      Joi.object().keys({
        actionName: Joi.string().required(),
        endpoint: Joi.string().required(),
        params: Joi.array().items( Joi.string().optional() ).required(),
        method: Joi.string().required()
      })
    ).required()
  }

  const validation = Joi.validate(req.body, schema);
  if(validation.error){
    return res.status(400).json(validation.error.details[0].message);
  }

  const _id = servicesList[servicesList.length-1]._id + 1;

  if( !!servicesList.find((service)=>(service.type === req.body.type)) ){
    return res.status(400).send("Srvice already exists");
  }
  const service = Object.assign({ _id }, req.body);

  servicesList.push(service);
  return res.send(service);
});

router.put('/services/update/:id', (req, res) => {
  const schema = {
    description: Joi.string().optional(),
    host: Joi.string().optional(),
    actions: Joi.array().items(
      Joi.object().keys({
        actionName: Joi.string().required(),
        endpoint: Joi.string().required(),
        params: Joi.array().items( Joi.string().optional() ).required(),
        method: Joi.string().required()
      })
    ).optional()
  }

  const validation = Joi.validate(req.body, schema);
  if(validation.error){
    return res.status(400).json(validation.error.details[0].message);
  }

  const service = servicesList.find( (service)=>(service._id === +req.params.id) );
  if( !service ){
    return res.status(400).send("Srvice doesn't exists");
  }

  Object.keys(req.body).forEach((key)=>{
    servicesList[servicesList.indexOf(service)][key] = req.body[key]
  });

  return res.send(service);
});



router.post('/requestService', (req, res) => {
  const schema = {
    type: Joi.string().required(),
    actionName: Joi.string().required(),
    params: Joi.array().items( Joi.string().optional() ).required(),
    details: Joi.object().optional()
  }

  const validation = Joi.validate(req.body, schema);
  if(validation.error){
    return res.status(400).json(validation.error.details[0].message);
  }

  const service = servicesList.find( (service)=>(service.type === req.body.type) );
  if( !service ){
    return res.status(400).send("Srvice doesn't exists");
  }

  const serviceAction = service.actions.find( (action)=>(action.actionName === req.body.actionName) );
  if( !serviceAction ){
    return res.status(400).send("serviceAction doesn't exists");
  }
  let url = `${service.host}${serviceAction.endpoint}`;
  if(req.body.params.length>0){
    req.body.params.forEach((param)=>{
      url += param;
    });
  }
  axios[serviceAction.method](url, req.body.details).then((response)=>{
    res.send(response.data);
  }).catch((error)=>{
    console.log(error);
    res.status(400).send("An error occured");
  })
});


module.exports = router;
